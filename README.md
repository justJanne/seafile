Seafile Docker Image for k8s
============================

TODO: Avatar volume

You first need to manually use the normal seafile docker image to generate the mykey.peer,
config files, the superuser, and migrate the databases.

If you additionally wish to use PostgreSQL, you’ll also need to run
`python3 manage.py createsuperuser`.

Then you can run this image, and mount the configuration files.


Volume Mounts:
--------------

Empty Directory

* /tmp

Persistent Volume

* /conf
* /data

Configuration Files

* /conf/ccnet.conf
* /conf/seafile.conf
* /conf/seahub_settings.py

For kubernetes, the config would look like this:

```yaml
kind: ConfigMap
apiVersion: v1
metadata:
  name: seafile-configs
  namespace: default
  labels:
    app: seafile
data:
  ccnet.conf: |-
    [General]
    SERVICE_URL = http://my.domain.tld
    
    [Database]
    ENGINE = pgsql
    HOST = database
    PORT = 5432
    USER = seafile
    PASSWD = seafile
    DB = ccnet
    CREATE_TABLES = true
  
    [Client]
    PORT = 13419
    UNIX_SOCKET = /tmp/seafile.sock
  seafile.conf: |-
    [fileserver]
    port = 8082
  
    [database]
    type = pgsql
    host = database
    port = 5432
    user = seafile
    password = seafile
    db_name = seafile
    create_tables = true
  seahub_settings.py: |-
    FILE_SERVER_ROOT = 'http://my.domain.tld/seafhttp'
    DATABASES = {
      'default': {
        'ENGINE': 'django.db.backends.postgresql',
        'NAME': 'seahub',
        'USER': 'seafile',
        'PASSWORD': 'seafile',
        'HOST': 'database',
        'PORT': '5432',
      }
    }
```

```yaml
kind: Secret
apiVersion: v1
metadata:
  name: seafile-key
  namespace: default
  labels:
    app: seafile
data:
  mykey.peer: |-
    -----BEGIN RSA PRIVATE KEY-----
    # Your data here
    -----END RSA PRIVATE KEY-----
```

```yaml
kind: PersistentVolumeClaim
apiVersion: v1
metadata:
  name: seafile
  namespace: default
  labels:
    app: seafile
spec:
  accessModes:
  - ReadWriteOnce
  resources:
    requests:
      storage: 300Gi
  storageClassName: default
```

```yaml
kind: Deployment
apiVersion: apps/v1
metadata:
  name: seafile
  namespace: default
  labels:
    app: seafile
spec:
  replicas: 1
  selector:
    matchLabels:
      app: seafile
  template:
    metadata:
      name: seafile
      labels:
        app: seafile
    spec:
      volumes:
      - name: data
        persistentVolumeClaim:
          claimName: seafile
      - name: config
        configMap:
          name: seafile-configs
      - name: key
        secret:
          secretName: seafile-key
      - name: tmp
        emptyDir: {}
      containers:
      - name: server
        image: k8r.eu/justjanne/seafile:server
        ports:
        - name: seafhttp
          containerPort: 8082
          protocol: TCP
        volumeMounts:
        - name: tmp
          mountPath: /tmp
        - name: data
          mountPath: /data
        - name: config
          mountPath: /conf
        - name: key
          mountPath: /conf/mykey.peer
          subPath: mykey.peer
      - name: seahub
        image: k8r.eu/justjanne/seafile:seahub
        ports:
          - name: seahub
            containerPort: 8000
            protocol: TCP
        volumeMounts:
          - name: tmp
            mountPath: /tmp
          - name: data
            mountPath: /data
          - name: config
            mountPath: /conf
          - name: key
            mountPath: /conf/mykey.peer
            subPath: mykey.peer
```

```yaml
kind: Service
apiVersion: v1
metadata:
  name: seafile
  namespace: default
  labels:
    app: seafile
spec:
  ports:
  - name: seahub
    protocol: TCP
    port: 8000
    targetPort: seahub
  - name: seafhttp
    protocol: TCP
    port: 8082
    targetPort: seafhttp
  selector:
    app: seafile
  type: ClusterIP
```

```yaml
kind: Ingress
apiVersion: networking.k8s.io/v1
metadata:
  annotations:
    ingress.kubernetes.io/proxy-body-size: "0"
  name: seafile
  namespace: default
spec:
  ingressClassName: "nginx"
  rules:
  - host: cloud.example.com
    http:
      paths:
        - path: "/"
          pathType: "Prefix"
          backend:
            service:
              name: seafile
              port:
                name: seahub
        - path: "/seafhttp"
          pathType: "Prefix"
          backend:
            service:
              name: seafile
              port:
                name: seafhttp
  tls:
  - hosts:
    - cloud.example.com
    secretName: seafile-tls
```
